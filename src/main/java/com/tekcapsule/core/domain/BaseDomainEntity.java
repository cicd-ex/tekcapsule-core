package com.tekcapsule.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public abstract class BaseDomainEntity {

    @JsonIgnore
    private String addedBy;
    @JsonIgnore
    private String updatedBy;
    @JsonIgnore
    private String addedOn;
    @JsonIgnore
    private String updatedOn;
}
